import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement
@XmlSeeAlso({SubGrupTeoria.class, SubGrupLaboratori.class})
public abstract class SubGrup implements ItemLlista{
	@XmlElement
	protected String idSubGrup;
	@XmlElement
	private int numEstudiants;
	@XmlElement
	private Assignatura assignatura;
	@XmlElement
	private Classe classe;
	@XmlElement
	private String idGrup;

	public SubGrup() {
		
	}
	
	public SubGrup(String idSubGrup, int numEstudiants, Assignatura assignatura, Grup grup) {
		this.idSubGrup = idSubGrup;
		this.numEstudiants = numEstudiants;
		this.assignatura = assignatura;
		this.idGrup = (String) grup.getId();
		classe = null;
	}
	
	public Assignatura getAssignatura() {
		return assignatura;
	}

	public Classe getClasse() {
		return classe;
	}

	public void assignarClasse(Classe classe) {
		this.classe = classe;
	}
	
	public String getIdGrup() {
		return idGrup;
	}
	
	public boolean esPractiques() {
		return (idSubGrup.length() > 7);
	}
	
	@Override
	public String getId() {
		return idSubGrup;
	}
	
	public String codisubgrup() {
		return idSubGrup.substring(3,5);
	}
}
