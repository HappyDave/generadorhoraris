import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Ambit implements ItemLlista{
	@XmlElement
	private String idAmbit;
	@XmlElementWrapper(name = "llista")
	@XmlElement(name = "Codi")
	private ArrayList<Codi> llistaCodis;
	
	public Ambit() {
		
	}
	
	public Ambit(String idAmbit) {
		this.idAmbit = idAmbit;
		llistaCodis = new ArrayList<Codi>();
	}
	
	@Override
	public String getId() {
		return idAmbit;
	}

	public Codi obtenirCodi(Curs c) {
		String idCodi = idAmbit + c.getId(); 
		Codi co = null;
		Iterator<Codi> itr = llistaCodis.iterator();
		
		while(itr.hasNext() && co == null) {
			Codi coAux = itr.next();
			if(coAux.getIdCodi().equals(idCodi)) co = coAux;
		}
		
		if(co == null) {
			co = new Codi(idCodi);
			llistaCodis.add(co);
		}
		
		return co;
	}

}
