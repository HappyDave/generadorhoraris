import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class GeneradorHoraris {
	private Titulacio t;

	private Magatzem<Curs> mCursos;
	private Magatzem<Titulacio> mTitulacions;
	private Magatzem<Assignatura> mAssignatures;
	private Magatzem<Ambit> mAmbits;
	private Magatzem<Aula> mAulesTeoria;
	private Magatzem<Aula> mAulesLaboratori;
	private Magatzem<Aula> mAulesInformatica;
	private Magatzem<RangData> mRangDates;
	private Magatzem<Grup> mGrups;

	private int idAssignatura;

	public GeneradorHoraris() throws JAXBException, FileNotFoundException {
		mCursos = new Magatzem<Curs>("Cursos.xml");
		mTitulacions = new Magatzem<Titulacio>("Titulacions.xml");
		mAssignatures = new Magatzem<Assignatura>("Assignatures.xml");
		mAmbits = new Magatzem<Ambit>("Ambits.xml");
		mAulesTeoria = new Magatzem<Aula>("AulesTeoria.xml");
		mAulesLaboratori = new Magatzem<Aula>("AulesLaboratori.xml");
		mAulesInformatica = new Magatzem<Aula>("AulesInformatica.xml");
		mRangDates = new Magatzem<RangData>("RangDates.xml");
		mGrups = new Magatzem<Grup>("Grups.xml");

		t = null;
		Assignatura aAux = mAssignatures.getLast();
		if(aAux == null) idAssignatura = 0;
		else idAssignatura = (int) aAux.getId(); 
	}

	public void crearTitulacio(String nom) throws JAXBException {
		Titulacio tLocal = new Titulacio(nom);
		mTitulacions.afegir(tLocal);
	}

	public void crearAula(String idAula) throws JAXBException {
		Aula aLocal = new Aula(idAula);
		String tipus = idAula.substring(1, 2);

		if(tipus.equals("L")) {
			mAulesLaboratori.afegir(aLocal);
		}
		else if(tipus.equals("I")) {
			mAulesInformatica.afegir(aLocal);
		}
		else {
			mAulesTeoria.afegir(aLocal);
		}
	}

	public void crearAssignatura(String sigles, String nom, int numPlaces, int idCurs, String idAmbit, String practiques) throws JAXBException {
		Curs c = mCursos.obtenir(idCurs);
		if(c == null) {
			c = new Curs(idCurs);
			mCursos.afegir(c);
		}

		Ambit am = mAmbits.extreure(idAmbit);
		if(am == null) am = new Ambit(idAmbit);
		int id = nouIdAssignatura();
		Assignatura a = new Assignatura(id, nom, sigles, numPlaces, practiques);
		Codi co = am.obtenirCodi(c);
		ArrayList<Grup> llistaGrups = (ArrayList<Grup>) mGrups.obtenirTots();

		int numEstudiants = numPlaces;
		int codi = 0;
		while(numEstudiants > 0) {
			String idGrup = co.getIdCodi() + codi;

			Grup g = null;
			Iterator<Grup> itr = llistaGrups.iterator();

			while(itr.hasNext() && g == null) {
				Grup gAux = itr.next();
				if(gAux.getId().equals(idGrup)) g = gAux;
			}

			if(g == null) {
				g = new Grup(idGrup);
				llistaGrups.add(g);
			}

			numEstudiants = g.crearSubGrups(a, numEstudiants);

			codi++;
		}
		mGrups.marshal();
		mAssignatures.afegir(a);
		mAmbits.afegir(am);
	}

	private int nouIdAssignatura() {
		idAssignatura++;
		return idAssignatura;
	}

	public void iniciAssignarGrups(String nom) {
		t = mTitulacions.obtenir(nom);
	}

	public void assignarGrup(String idGrup) {
		Grup g = mGrups.obtenir(idGrup);
		t.assignarGrup(g);
	}

	public void fiAssignarGrups() throws JAXBException {
		mTitulacions.marshal();
		t = null;
	}

	private void ocuparClasse(SubGrup subGrup, Classe classe) {
		subGrup.assignarClasse(classe);
		classe.assignarSubGrup(subGrup);
	}

	public void generarClasses() throws JAXBException {
		ArrayList<RangData> llistaRangDates = (ArrayList<RangData>) mRangDates.obtenirTots();
		ArrayList<Aula> llistaAules = new ArrayList<Aula>(); 
		llistaAules.addAll((ArrayList<Aula>) mAulesTeoria.obtenirTots());
		llistaAules.addAll((ArrayList<Aula>) mAulesLaboratori.obtenirTots());
		llistaAules.addAll((ArrayList<Aula>) mAulesInformatica.obtenirTots());

		for (RangData rangData: llistaRangDates) {
			rangData.generarClasses(llistaAules);
		}

		mAulesTeoria.marshal();
		mAulesLaboratori.marshal();
		mAulesInformatica.marshal();
	}

	public boolean checkIt (Classe c, SubGrup sg) {
		Grup g = mGrups.obtenir(sg.getIdGrup());
		boolean a = noSolapa(g, c, sg);
		boolean p = dinsFranja(g, c);
		boolean aux = (a && p);
		return aux;
	}

	public boolean noSolapa (Grup g, Classe c, SubGrup sg) {
		Boolean ret = true;
		ArrayList<SubGrup> Laux = g.getLlistaSubGrups();
		Iterator<SubGrup> itList = Laux.iterator();
		SubGrup sgaux;
		while(ret && itList.hasNext() ) {
			sgaux = itList.next();
			if ((sgaux.getClasse() != null) && (c.getRangData().getDia() == sgaux.getClasse().getRangData().getDia()) && (c.getRangData().getHora() == sgaux.getClasse().getRangData().getHora())) {
				if ((sgaux.getId().length() == 7) || (sg.getId().length() == 7)) ret = false; 	
			}
		}
		return ret;
	}

	public boolean dinsFranja(Grup g, Classe c) {
		return g.getFranja() == c.getFranja();
	}

	public void generarHoraris() {          
		boolean solucio = false, trobatTeoria;
		int countTeoria = 0;
		ArrayList<Classe> llistaClassesInformatica = new ArrayList<Classe>();
		for(Aula aula : mAulesInformatica.obtenirTots()) {
			llistaClassesInformatica.addAll(aula.getLlistaClasses());
		}
		ArrayList<Classe> llistaClassesLaboratori = new ArrayList<Classe>();
		for(Aula aula : mAulesLaboratori.obtenirTots()) {
			llistaClassesLaboratori.addAll(aula.getLlistaClasses());
		}
		ArrayList<Classe> llistaClassesTeoria = new ArrayList<Classe>();
		for(Aula aula : mAulesTeoria.obtenirTots()) {
			llistaClassesTeoria.addAll(aula.getLlistaClasses());
		}

		ArrayList<Grup> llistaGrups = new ArrayList<Grup>(mGrups.obtenirTots());
		ArrayList<SubGrup> llistaSubGrups = new ArrayList<SubGrup>();
		for (Grup grup : llistaGrups) {
			llistaSubGrups.addAll(grup.getLlistaSubGrups());
		}

		ArrayList<Classe> llistaClasse = null;
		while (!llistaSubGrups.isEmpty()) {
			solucio = false;
			trobatTeoria = false;
			int randomSubGrups = (int)(Math.random() * llistaSubGrups.size());
			SubGrup subgrup = llistaSubGrups.get(randomSubGrups); // agafa subgrup	
			while (!trobatTeoria && countTeoria < 158) {
				randomSubGrups = (int)(Math.random() * llistaSubGrups.size());
				subgrup = llistaSubGrups.get(randomSubGrups); // agafa subgrup
				if (!subgrup.esPractiques()) {
					countTeoria ++;
					trobatTeoria = true;
				}
			}

			if (subgrup.esPractiques()) { // laboratori consultar assignatura del subgrup --> se l'ambit
				if(subgrup.getAssignatura().getPractiques().equals("I")) {
					llistaClasse = llistaClassesInformatica;
				}
				else if (subgrup.getAssignatura().getPractiques().equals("L")){
					llistaClasse = llistaClassesLaboratori;
				}
				else {
					llistaClasse = llistaClassesTeoria;
				}
			}
			else {
				llistaClasse = llistaClassesTeoria;
			}
			Classe classe = null;
			int randomClasse = 0;
			while (!solucio) {
				randomClasse = (int)(Math.random() * llistaClasse.size());
				classe = llistaClasse.get(randomClasse);
				solucio = checkIt(classe, subgrup); 	
			}      
			this.ocuparClasse(subgrup, classe);
			llistaSubGrups.remove(randomSubGrups);
			llistaClasse.remove(randomClasse);
		}
	}

	//MAIN
	public static void main(String[] args) throws JAXBException, FileNotFoundException {
		GeneradorHoraris GH = new GeneradorHoraris();
		GH.generarHoraris();
		
		// printa tots subgrups
		 GH.printGrups();
		
		// printa un unic subgrup
		// String idgrup = "I30";
		// GH.printGrup(idgrup);
		
		// print totes les aules
		// GH.printAulesInformatica();
		
		// print totes les aules teoria
		// GH.printAulesTeoria();
		
		// print totes les aules laboratori
		// GH.printAulesLaboratori();
		
		// print totes les aules de informatica
		// GH.printAulesInformatica();
	}

	public void printGrups() {
		String dia[] = {"dilluns", "dimarts", "dimecres", "dijous", "divendres"};
		for (Grup grup : mGrups.obtenirTots()) {
			System.out.println("--> " + grup.getId() + ":");
			for (SubGrup subGrup : grup.getLlistaSubGrups()) {
				System.out.println("* " + subGrup.getId() + " -> Aula: " + subGrup.getClasse().getIdAula() + " -> dia/hora: " + dia[subGrup.getClasse().getRangData().getDia()-1] + " " + subGrup.getClasse().getRangData().getHora() + ":30 h");
			}
		}
	}

	public void printGrup(String idgrup) {
		String dia[] = {"dilluns", "dimarts", "dimecres", "dijous", "divendres"};
		Grup grup = mGrups.obtenir(idgrup);
		System.out.println(grup.getId());
		for (SubGrup subGrup : grup.getLlistaSubGrups()) {
				System.out.println("* " + subGrup.getId() + " -> Aula: " + subGrup.getClasse().getIdAula() + " -> dia/hora: " + dia[subGrup.getClasse().getRangData().getDia()-1] + " " + subGrup.getClasse().getRangData().getHora() + ":30 h");
		}
	}
	
	public void printAulesTeoria() {
		String dia[] = {"dilluns", "dimarts", "dimecres", "dijous", "divendres"};
		for (Aula aula : mAulesTeoria.obtenirTots()) {
			System.out.println("--> " + aula.getId() + ":");
			for (Classe classe : aula.getLlistaClasses()) {
				System.out.println("Grup: " + classe.getIdSubGrup() + " -> dia/hora: " + dia[classe.getRangData().getDia()-1] + " " + classe.getRangData().getHora() + ":30 h");
			}
		}
	}
	
	public void printAulesLaboratori() {
		String dia[] = {"dilluns", "dimarts", "dimecres", "dijous", "divendres"};
		for (Aula aula : mAulesLaboratori.obtenirTots()) {
			System.out.println("--> " + aula.getId() + ":");
			for (Classe classe : aula.getLlistaClasses()) {
				System.out.println("Grup: " + classe.getIdSubGrup() + " -> dia/hora: " + dia[classe.getRangData().getDia()-1] + " " + classe.getRangData().getHora() + ":30 h");
			}
		}
	}
	
	public void printAulesInformatica() {
		String dia[] = {"dilluns", "dimarts", "dimecres", "dijous", "divendres"};
		for (Aula aula : mAulesInformatica.obtenirTots()) {
			System.out.println("--> " + aula.getId() + ":");
			for (Classe classe : aula.getLlistaClasses()) {
				System.out.println("Grup: " + classe.getIdSubGrup() + " -> dia/hora: " + dia[classe.getRangData().getDia()-1] + " " + classe.getRangData().getHora() + ":30 h");
			}
		}
	}
	
	public void printAules() {
		ArrayList<Aula> llistaAules = (ArrayList<Aula>) mAulesTeoria.obtenirTots();
		llistaAules.addAll((ArrayList<Aula>) mAulesLaboratori.obtenirTots());
		llistaAules.addAll((ArrayList<Aula>) mAulesInformatica.obtenirTots());
		Iterator<Aula> itau = llistaAules.iterator();
		Aula aula;
		while (itau.hasNext()) {
			aula = itau.next();
			System.out.println("Aula: " + aula.getId());
			ArrayList<Classe> classe = aula.getLlistaClasses();
			Iterator<Classe> itcl = classe.iterator();
			Classe c;
			while(itcl.hasNext()) {
				c = itcl.next();
				if(c.getIdSubGrup() != null){
					System.out.println("Classe: " + c.getIdSubGrup() + " / horari: " + c.getRangData().getId());
				}
			}
		}
	}

	private void generarAules() throws JAXBException {

		this.crearAula("AA201");
		this.crearAula("AA202");
		this.crearAula("AA203");
		this.crearAula("AA204");
		this.crearAula("AA205");
		this.crearAula("AA206");
		this.crearAula("AA207");
		this.crearAula("AA208");
		this.crearAula("AA209");
		this.crearAula("AA210");

		this.crearAula("BA001");
		this.crearAula("BA003");
		this.crearAula("BA005");
		this.crearAula("BA007");
		this.crearAula("BA002");
		this.crearAula("BA004");
		this.crearAula("BA006");
		this.crearAula("BA008");
		this.crearAula("BA102");
		this.crearAula("BA103");
		this.crearAula("BA107");
		this.crearAula("BA108");
		this.crearAula("BA109");
		this.crearAula("BA110");
		this.crearAula("BA111");

		this.crearAula("AI101s1");
		this.crearAula("AI103s1");
		this.crearAula("AI105s1");
		this.crearAula("AI107s1");
		this.crearAula("AI109s1");
		this.crearAula("AI111s1");
		this.crearAula("AI112s1");
		this.crearAula("AI114s1");
		this.crearAula("AI115s1");
		this.crearAula("AI117s1");
		this.crearAula("AI119s1");

		this.crearAula("AI101s2");
		this.crearAula("AI103s2");
		this.crearAula("AI105s2");
		this.crearAula("AI107s2");
		this.crearAula("AI109s2");
		this.crearAula("AI111s2");
		this.crearAula("AI112s2");
		this.crearAula("AI114s2");
		this.crearAula("AI115s2");
		this.crearAula("AI117s2");
		this.crearAula("AI119s2");


		this.crearAula("AL002s1");
		this.crearAula("AL007s1");
		this.crearAula("AL010s1");
		this.crearAula("AL011s1");
		this.crearAula("AL012s1");
		this.crearAula("AL013s1");
		this.crearAula("AL014s1");
		this.crearAula("AL019s1");
		this.crearAula("AL102s1");
		this.crearAula("AL103s1");
		this.crearAula("AL104s1");
		this.crearAula("AL106s1");
		this.crearAula("AL107s1");
		this.crearAula("AL114s1");
		this.crearAula("AL116s1");
		this.crearAula("AL118s1");


		this.crearAula("AL002s2");
		this.crearAula("AL007s2");
		this.crearAula("AL010s2");
		this.crearAula("AL011s2");
		this.crearAula("AL012s2");
		this.crearAula("AL013s2");
		this.crearAula("AL014s2");
		this.crearAula("AL019s2");
		this.crearAula("AL102s2");
		this.crearAula("AL103s2");
		this.crearAula("AL104s2");
		this.crearAula("AL106s2");
		this.crearAula("AL107s2");
		this.crearAula("AL114s2");
		this.crearAula("AL116s2");
		this.crearAula("AL118s2");
	}

	private void generarAssignaturesInformatica() throws JAXBException {
		this.crearAssignatura("FOMA", "FONAMENTS MATEMATICS", 60, 1, "I", "I");
		this.crearAssignatura("FISI", "FISICA", 60, 1, "I", "L");
		this.crearAssignatura("FOPR", "FONAMENTS DE PROGRAMACIO", 60, 1, "I", "I");
		this.crearAssignatura("INCO", "INTRODUCCIO ALS COMPUTADORS", 60, 1, "I", "I");

		this.crearAssignatura("LOAL", "LOGICA I ALGEBRA", 60, 2, "I", "I");
		this.crearAssignatura("MATD", "MATEMATICA DISCRETA", 60, 2, "I", "I");
		this.crearAssignatura("PRO1", "PROGRAMACIO I", 60, 2, "I", "I");
		this.crearAssignatura("ESC1", "ESTRUCTURA DE COMPUTADORS I", 60, 2, "I", "I");

		this.crearAssignatura("ESIN", "ESTRUCTURA DE LA INFORMACIO", 60, 3, "I", "I");
		this.crearAssignatura("INEP", "INTRODUCCIO A ENGINYERIA DEL PROGRAMARI", 60, 3, "I", "I");
		this.crearAssignatura("ESC2", "ESTRUCTURA DE COMPUTADORS II", 60, 3, "I", "I");
		this.crearAssignatura("EMPR", "EMPRESA", 60, 3, "I", "T");
		this.crearAssignatura("ESTA", "ESTADISTICA", 60, 3, "I", "I");

		this.crearAssignatura("EESO", "ECONOMIA, ETICA I SOCIETAT", 60, 5, "I", "T");
		this.crearAssignatura("SODX", "SISTEMES OPERATIUS DISTRIBUITS I EN XARXA", 60, 5, "I", "I");
		this.crearAssignatura("ADSO", "ADMINISTRACIO DE SISTEMES OPERATIUS", 60, 5, "I", "I");
		this.crearAssignatura("PACO", "PARALELISME I CONCURRENCIA", 60, 5, "I", "I");
		this.crearAssignatura("INTE", "INTERNET", 60, 5, "I", "I");
	}
	
	private void generarAssignaturesSistemesElectronics() throws JAXBException {
		this.crearAssignatura("FOMA", "FONAMENTS MATEMATICS", 60, 1, "T", "I");
		this.crearAssignatura("FISI", "FISICA", 60, 1, "T", "L");
		this.crearAssignatura("FOPR", "FONAMENTS DE PROGRAMACIO", 60, 1, "T", "I");
		this.crearAssignatura("INCO", "INTRODUCCIO ALS COMPUTADORS", 60, 1, "T", "I");
		this.crearAssignatura("SOST", "SOSTENIBILITAT", 60, 1, "T", "T");

		this.crearAssignatura("CAAV", "CALCULA AVANCAT", 60, 2, "T", "I");
		this.crearAssignatura("FOEL", "FONAMENTS ELECTRONICA", 60, 2, "T", "L");
		this.crearAssignatura("AMFI", "AMPLIACIO DE FISICA", 60, 2, "T", "L");
		this.crearAssignatura("MATE", "MATEMATIQUES DE LES TELECOMUNICACIONS", 60, 2, "T", "I");
		this.crearAssignatura("ACIN", "ACCESIBILITAT I INNOVACIO", 60, 2, "T", "T");

		this.crearAssignatura("COCE", "COMPONENTS I CIRCUITS ELECTRONICS", 60, 3, "T", "L");
		this.crearAssignatura("ANCE", "ANALISI DE CIRCUITS", 60, 3, "T", "L");
		this.crearAssignatura("ELDI", "ELECTRONICA DIGITAL", 60, 3, "T", "L");
		this.crearAssignatura("EMPR", "EMPRESA", 60, 3, "T", "T");
		this.crearAssignatura("ESTA", "ESTADISTICA", 60, 3, "T", "I");

		this.crearAssignatura("SIDP", "SISTEMES DIGITALS PROGRAMABLES", 60, 5, "T", "L");
		this.crearAssignatura("TERM", "TECNIQUES DE RADIOFREQUENCIA I MICROONES", 60, 5, "T", "L");
		this.crearAssignatura("ELPO", "ELECTRONICA DE POTENCIA", 60, 5, "T", "L");
		this.crearAssignatura("SIER", "SISTEMES DE MISIO I RECEPCIO", 60, 5, "T", "L");
		this.crearAssignatura("INTE", "INTERNET", 60, 5, "T", "I");
	}

	private void generarAssignaturesMecanica() throws JAXBException {
		this.crearAssignatura("FOMA", "FONAMENTS MATEMATICS", 300, 1, "N", "I");
		this.crearAssignatura("FIS1", "FISICA 1", 300, 1, "N", "L");
		this.crearAssignatura("INFO", "INFORMATICA", 300, 1, "N", "I");
		this.crearAssignatura("QUIM", "QUIMICA", 300, 1, "N", "T");
		this.crearAssignatura("SOST", "SOSTENIBILITAT", 300, 1, "N", "T");

		this.crearAssignatura("CAAV", "CALCULA AVANCAT", 180, 2, "N", "I");
		this.crearAssignatura("EXGR", "EXPRESIO GRAFICA", 180, 2, "N", "I");
		this.crearAssignatura("FIS2", "FISICA 2", 180, 2, "N", "L");
		this.crearAssignatura("EQDI", "EQUACIONS DIFERENCIALS", 180, 2, "N", "I");
		this.crearAssignatura("ACIN", "ACCESIBILITAT I INNOVACIO", 180, 2, "N", "T");

		this.crearAssignatura("SIEL", "SISTEMES ELECTRICS", 180, 3, "N", "L");
		this.crearAssignatura("CIMA", "CIENCIES DELS MATERIALS", 180, 3, "N", "L");
		this.crearAssignatura("ETMF", "ENGINYERIA TERMICA I MECANICA DE FLUIDS", 180, 3, "N", "L");
		this.crearAssignatura("EMPR", "EMPRESA", 180, 3, "N", "T");
		this.crearAssignatura("ESTA", "ESTADISTICA", 180, 3, "N", "I");

		this.crearAssignatura("TEMA", "TEORIA DE MAQUINES", 60, 5, "M", "T");
		this.crearAssignatura("MAES", "MATERIALS ESTRUCTURALS", 60, 5, "M", "L");
		this.crearAssignatura("RMA1", "RESISTENCIA DE MATERIALS 1", 60, 5, "M", "T");
		this.crearAssignatura("RMA2", "RESISTENCIA DE MATERIALS 2", 60, 5, "M", "T");
		this.crearAssignatura("EXG2", "EXPRESIO GRAFICA 2", 60, 5, "M", "I");

		this.crearAssignatura("ENSU", "ENGINYERIA DE SUPERFICIES", 60, 7, "M", "T");
		this.crearAssignatura("DMAO", "DISSENY DE MAQUINES ASSISTIT PER ORDINADOR", 60, 7, "M", "I");
		this.crearAssignatura("TEEE", "TECNIQUES DESCRIPTURA PER A LENGINYERIA", 60, 7, "M", "T");
		this.crearAssignatura("TCAP", "TECNIQUES DE COMUNICACIO ACADEMIQUES I PROFESSIONALS", 60, 7, "M", "T");
		this.crearAssignatura("TESA", "TECNIQUES EXPERIMENTALS I DE SIMULACIO DANALISI DE TENSIONS", 60, 7, "M", "L");
		this.crearAssignatura("MATH", "MAQUINES TERMIQUES I HIDRAULIQUES", 60, 7, "M", "L");
		this.crearAssignatura("DSAO", "DISSENY I SIMULACIO ASSISTITS PER ORDINADOR", 60, 7, "M", "I");
	}

	private void generarAssignaturesEectrica() throws JAXBException {
		this.crearAssignatura("LIEL", "LINIES ELECTRIQUES", 60, 5, "E", "L");
		this.crearAssignatura("ELPO", "ELECTRONICA DE POTENCIA", 60, 5, "E", "T");
		this.crearAssignatura("CIEL", "CIRCUITS ELECTRICS", 60, 5, "E", "T");
		this.crearAssignatura("MAE1", "MAQUINES ELECTRIQUES 1", 60, 5, "E", "L");
		this.crearAssignatura("REAU", "REGULACIO AUTOMATICA", 60, 5, "E", "T");

		this.crearAssignatura("VEEH", "VEHICLES ELECTRICS I HIBRIDS", 60, 7, "E", "L");
		this.crearAssignatura("DMDE", "DISSENY DE MAQUINES I DISPOSITIUS ELECRICS", 60, 7, "E", "I");
		this.crearAssignatura("GSEP", "GESTIO DE SISTEMES ELECTRICS DE POTENCIA I ESTALVI DENERGIA ELECTRICA", 60, 7, "E", "T");
		this.crearAssignatura("ACEL", "ACCIONAMENTS ELECTRICS", 60, 7, "E", "L");
		this.crearAssignatura("TEEE", "TECNIQUES DESCRIPTURA PER A LENGINYERIA", 60, 7, "E", "T");
		this.crearAssignatura("TCAP", "TECNIQUES DE COMUNICACIO ACADEMIQUES I PROFESSIONALS", 60, 7, "E", "T");
		this.crearAssignatura("TMDM", "TECNIQUES DE MANTENIMENT I DIAGNOSTIC EN MOTORS I ACCIONAMENTS ELECTRICS", 60, 7, "E", "T");
		this.crearAssignatura("SIFE", "SISTEMES FOTOVOLTAICS I EOLICS", 60, 7, "E", "L");
		this.crearAssignatura("LUMI", "LUMINOTECNIA", 60, 7, "E", "L");
	}
	
	private void generarAssignaturesElecAutomat() throws JAXBException {
		this.crearAssignatura("AUIN", "AUTOMATITZACIO INDUSTRIAL", 60, 5, "A", "L");
		this.crearAssignatura("ININ", "INFORMATICA INDUSTRIAL", 60, 5, "A", "I");
		this.crearAssignatura("ELAN", "ELECTRONICA ANALOGICA", 60, 5, "A", "L");
		this.crearAssignatura("ELEC", "ELECTROTECNIA", 60, 5, "A", "T");
		this.crearAssignatura("ELDI", "ELECTRONICA DIGITAL", 60, 5, "A", "I");

		this.crearAssignatura("ENRE", "ENERGIES RENOVABLES", 60, 7, "A", "T");
		this.crearAssignatura("ENCO", "ENGINYERIA DE CONTROL", 60, 7, "A", "T");
		this.crearAssignatura("TEEE", "TECNIQUES DESCRIPTURA PER A LENGINYERIA", 60, 7, "A", "T");
		this.crearAssignatura("TCAP", "TECNIQUES DE COMUNICACIO ACADEMIQUES I PROFESSIONALS", 60, 7, "A", "T");
		this.crearAssignatura("SIPI", "SISTEMES DE PRODUCCIO INTEGRATS", 60, 7, "A", "T");
		this.crearAssignatura("SDIN", "SISTEMES DISTRIBUITS INDUSTRIALS", 60, 7, "A", "I");
		this.crearAssignatura("SIIN", "SISTEMES DINSTRUMENTACIO", 60, 7, "A", "I");
	}
	
	private void generarAssignaturesDisseny() throws JAXBException {
		this.crearAssignatura("MADI", "MATEMATIQUES DEL DISENY", 120, 2, "D", "I");
		this.crearAssignatura("EXGR", "EXPRESIO GRAFICA", 120, 2, "D", "I");
		this.crearAssignatura("FIS2", "FISICA 2", 120, 2, "D", "L");
		this.crearAssignatura("ESTE", "ESTETICA", 120, 2, "D", "L");
		this.crearAssignatura("ACIN", "ACCESIBILITAT I INNOVACIO", 120, 2, "D", "T");

		this.crearAssignatura("MECA", "MECANICA", 120, 3, "D", "L");
		this.crearAssignatura("TAD1", "TALLER DE DISENY 1", 120, 3, "D", "L");
		this.crearAssignatura("EXAR", "EXPRESIO ARTISTICA", 120, 3, "D", "T");
		this.crearAssignatura("EMPR", "EMPRESA", 120, 3, "D", "T");
		this.crearAssignatura("ESTA", "ESTADISTICA", 120, 3, "D", "I");

		this.crearAssignatura("PRFA", "PROCESSOS DE FABRICACIO", 120, 5, "D", "I");
		this.crearAssignatura("SIEK", "SISTEMES ELECTRONICS", 120, 5, "D", "L");
		this.crearAssignatura("DIBA", "DISENY BASIC", 120, 5, "D", "T");
		this.crearAssignatura("DIGR", "DISENY GRAFIC", 120, 5, "D", "I");
		this.crearAssignatura("DIAO", "DISENY ASSISTIT PER ORDINADOR", 120, 5, "D", "I");

		this.crearAssignatura("TEEE", "TECNIQUES DESCRIPTURA PER A LENGINYERIA", 60, 7, "D", "T");
		this.crearAssignatura("INPS", "INTERACCIO PERSONA-SISTEMA", 60, 7, "D", "T");
		this.crearAssignatura("ENUA", "ENGINYERIA DE LA USABILITAT I LACCESSIBILITAT", 60, 7, "D", "T");
		this.crearAssignatura("FIPI", "FIABILITAT I INTEGRITAT DEL PRODUCTES INDUSTRIALS", 60, 7, "D", "T");
		this.crearAssignatura("DIDU", "DISSENY INCLUSIU I DISSENY CENTRAT EN LUSUARI", 60, 7, "D", "I");
		this.crearAssignatura("SEMA", "SELECCIO DE MATERIALS EN EL DISSENY INDUSTRIAL", 60, 7, "D", "L");
		this.crearAssignatura("DPMM", "DISSENY I PROTOTIP DE MOTLLES I MATRIUS", 60, 7, "D", "I");
		this.crearAssignatura("INPS", "INTERACCIO PERSONA-SISTEMA", 60, 7, "D", "T");
	}

	private void generarTitulacio() throws JAXBException {
		this.crearTitulacio("INFORMATICA");
		this.crearTitulacio("TELECOMUNICACIONS");
		this.crearTitulacio("INDUSTRIAL MECANICA");
		this.crearTitulacio("INDUSTRIAL ELECTRICA");
		this.crearTitulacio("INDUSTRIAL ELECTRONICA");
		this.crearTitulacio("INDUSTRIAL DISENY");
	}

	private void linkejaGrupTitulacio() throws JAXBException {

		this.iniciAssignarGrups("INFORMATICA");
		this.assignarGrup("I10");
		this.assignarGrup("I20");
		this.assignarGrup("I30");
		this.assignarGrup("I50");
		this.fiAssignarGrups();

		this.iniciAssignarGrups("TELECOMUNICACIONS");
		this.assignarGrup("T10");
		this.assignarGrup("T20");
		this.assignarGrup("T30");
		this.assignarGrup("T50");
		this.fiAssignarGrups();

		this.iniciAssignarGrups("INDUSTRIAL MECANICA");
		this.assignarGrup("N10");
		this.assignarGrup("N11");
		this.assignarGrup("N12");
		this.assignarGrup("N13");
		this.assignarGrup("N14");
		this.assignarGrup("N20");
		this.assignarGrup("N21");
		this.assignarGrup("N22");
		this.assignarGrup("N30");
		this.assignarGrup("N31");
		this.assignarGrup("N32");
		this.assignarGrup("M50");
		this.assignarGrup("M70");
		this.fiAssignarGrups();

		this.iniciAssignarGrups("INDUSTRIAL ELECTRICA");
		this.assignarGrup("N10");
		this.assignarGrup("N11");
		this.assignarGrup("N12");
		this.assignarGrup("N13");
		this.assignarGrup("N14");
		this.assignarGrup("N20");
		this.assignarGrup("N21");
		this.assignarGrup("N22");
		this.assignarGrup("N30");
		this.assignarGrup("N31");
		this.assignarGrup("N32");
		this.assignarGrup("E50");
		this.assignarGrup("E70");
		this.fiAssignarGrups();

		this.iniciAssignarGrups("INDUSTRIAL ELECTRONICA");
		this.assignarGrup("N10");
		this.assignarGrup("N11");
		this.assignarGrup("N12");
		this.assignarGrup("N13");
		this.assignarGrup("N14");
		this.assignarGrup("N20");
		this.assignarGrup("N21");
		this.assignarGrup("N22");
		this.assignarGrup("N30");
		this.assignarGrup("N31");
		this.assignarGrup("N32");
		this.assignarGrup("A50");
		this.assignarGrup("A70");
		this.fiAssignarGrups();

		this.iniciAssignarGrups("INDUSTRIAL DISENY");
		this.assignarGrup("N10");
		this.assignarGrup("N11");
		this.assignarGrup("N12");
		this.assignarGrup("N13");
		this.assignarGrup("N14");
		this.assignarGrup("D20");
		this.assignarGrup("D21");
		this.assignarGrup("D30");
		this.assignarGrup("D31");
		this.assignarGrup("D50");
		this.assignarGrup("D51");
		this.fiAssignarGrups();
	}

}