import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Classe {
	@XmlElement
	private RangData rangData;
	@XmlElement
	private String idSubGrup;
	@XmlElement
	private String idAula;
	
	
	public String getIdAula() {
		return idAula;
	}

	public Classe() {
		
	}
	
	public Classe(RangData rangData, Aula aula) {
		this.rangData = rangData;
		idSubGrup = null;
		this.idAula = (String) aula.getId();
	}
	
	public void assignarSubGrup(SubGrup subGrup) {
		this.idSubGrup = subGrup.getId();
	}

	public RangData getRangData() {
		return rangData;
	}
	
	public boolean getFranja() {
		boolean ret;
		if (rangData.getHora() < 15) ret = true;
		else ret = false;
		return ret;
	}

	public String getIdSubGrup() {
		return idSubGrup;
	}
}
