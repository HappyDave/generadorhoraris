import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;


public class Magatzem<T extends ItemLlista> {
	private String XML;
	private Llista<T> llistaItems;
	JAXBContext context;

	public Magatzem(String file) throws JAXBException, FileNotFoundException {
		XML = file;
		context = JAXBContext.newInstance(Llista.class);
		Unmarshaller um = context.createUnmarshaller();
		llistaItems = (Llista<T>) um.unmarshal(new FileReader(XML)); 
	}
	
	public T obtenir(Object id) {
		return llistaItems.obtenir(id);
	}
	
	public T extreure(Object id) {
		return llistaItems.extreure(id);
	}

	public void afegir(T item) throws JAXBException {
		llistaItems.afegir(item);
		marshal();
	}
	
	public T getLast() {
		return llistaItems.getLast();
	}

	public List<T> obtenirTots() {
		return llistaItems.obtenirTots();
	}

	public void marshal() throws JAXBException {
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.marshal(llistaItems, new File(XML));
	}

}
