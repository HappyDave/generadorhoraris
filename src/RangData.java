import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RangData implements ItemLlista{
	@XmlElement
	private String idRangData;
	@XmlElement
	private int dia;
	@XmlElement
	private int horaInici;
	
	public RangData() {
		
	}
	
	public RangData(int dia, int horaInici) {
		this.idRangData = dia + ", " + horaInici;
		this.dia = dia;
		this.horaInici = horaInici;
	}
	
	@Override
	public Object getId() {
		return idRangData;
	}

	public void generarClasses(ArrayList<Aula> llistaAules) {
		for (Aula aula : llistaAules) {
			aula.generarClasse(this);
		}
		
	}
	
	public int getHora () {
		return horaInici;
	}

	public int getDia() {
		return dia;
	}
}
