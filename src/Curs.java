import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Curs implements ItemLlista{
	
	@XmlElement
	private int idCurs;

	public Curs() {
		
	}
	
	public Curs(int idCurs) {
		this.idCurs = idCurs;
	}

	@Override
	public Object getId() {
		return idCurs;
	}

}
