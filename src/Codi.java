import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Codi {
	
	@XmlElement
	private String idCodi;
	
	public Codi() {
	
	}

	public Codi(String idCodi) {
		this.idCodi = idCodi;
	}

	public String getIdCodi() {
		return idCodi;
	}
}
