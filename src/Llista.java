import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "Llista")
public class Llista<T extends ItemLlista> {

	@XmlElementWrapper(name = "Items")
	@XmlElements({
	     @XmlElement(name="Ambit", type=Ambit.class),
	     @XmlElement(name="Curs", type=Curs.class),
	     @XmlElement(name="Titulacio", type=Titulacio.class),
	     @XmlElement(name="Assignatura", type=Assignatura.class),
	     @XmlElement(name="Aula", type=Aula.class),
	     @XmlElement(name="RangData", type=RangData.class),
	     @XmlElement(name="Grup", type=Grup.class)
	})
	private ArrayList<T> llistaItems;

	public Llista() {
		llistaItems = new ArrayList<T>();
	}
	
	public T obtenir(Object id) {
		T item = null;
		Iterator<T> itr = llistaItems.iterator();
		
		while(itr.hasNext() && item == null) {
			T itemAux = itr.next();
			if(itemAux.getId().equals(id)) item = itemAux;
		}

		return item;
	}

	public void afegir(T item) {
		llistaItems.add(item);
	}

	public T extreure(Object id) {
		T item = null;
		Iterator<T> itr = llistaItems.iterator();

		while(itr.hasNext() && item == null) {
			T itemAux = itr.next();
			if(itemAux.getId().equals(id)) {
				item = itemAux;
				itr.remove();
			}
		}
		return item;
	}
	
	public T getLast() {
		T item = null;
		if (!llistaItems.isEmpty()) {
			item = llistaItems.get(llistaItems.size() - 1);
		}
		return item;
	}
	
	public List<T> obtenirTots() {
		return llistaItems;
	}

}
