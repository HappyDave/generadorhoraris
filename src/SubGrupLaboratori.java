
public class SubGrupLaboratori extends SubGrup{
	
	public SubGrupLaboratori() {
		
	}
	
	public SubGrupLaboratori(String idGrup, Assignatura assig, int codi, int numEstudiants, Grup grup) {
		super(idGrup + codi + assig.getSigles(), numEstudiants , assig, grup);
	}

	public boolean mateixSG(SubGrup sgaux) {
		boolean mateix = false;
		String aux1 = idSubGrup.substring(3,5);
		String aux2 = sgaux.getId().substring(3,5);
		if (aux1.equals(aux2)) mateix = true;
		return mateix;
	}
}
