import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Titulacio implements ItemLlista{
	
	@XmlElement
	private String nom;
	@XmlElementWrapper(name = "llista")
	@XmlElement(name = "Grup")
	private ArrayList<Grup> llistaGrups;

	public ArrayList<Grup> getLlistaGrups() {
		return llistaGrups;
	}

	public Titulacio() {
		
	}
	
	public Titulacio(String nom) {
		this.nom = nom;
		llistaGrups = new ArrayList<Grup>();
	}

	@Override
	public Object getId() {
		return nom;
	}

	public void assignarGrup(Grup g) {
		llistaGrups.add(g);
	}

}
