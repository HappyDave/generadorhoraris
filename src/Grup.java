import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Grup implements ItemLlista{
	@XmlElement
	private String idGrup;
	@XmlElement
	private boolean mati;
	private int contadorSolapades;
	private int minSolapades;
	@XmlElementWrapper(name = "llista")
	@XmlElement(name = "SubGrup")
	private ArrayList<SubGrup> llistaSubGrups;
	

	private static final int maxEstudiantsTeoria = 60;
	private static final int maxEstudiantsLaboratori = 30;
	
	public Grup() {

	}

	public Grup(String idGrup) {
		this.idGrup = idGrup;
		
		if (Integer.parseInt(idGrup.substring(1)) == 20) mati = false;
		else {
			mati = (Integer.parseInt(idGrup.substring(2)) % 2 == 0);
		}
		
		llistaSubGrups = new ArrayList<SubGrup>();
		this.contadorSolapades = 0;
		this.minSolapades = 8;
		
	}

	public ArrayList<SubGrup> getLlistaSubGrups() {
		return llistaSubGrups;
	}

	@Override
	public Object getId() {
		return idGrup;
	}

	public int crearSubGrups(Assignatura a, int numEstudiants) {
		int numEstudiantsGrup = Math.min(numEstudiants, maxEstudiantsTeoria);
		SubGrupTeoria sgt = new SubGrupTeoria(idGrup, a, numEstudiantsGrup, this);
		llistaSubGrups.add(sgt);
		
		int codi = 10;
		while(numEstudiantsGrup > 0) {
			SubGrupLaboratori sgl = new SubGrupLaboratori(idGrup, a, codi, Math.min(numEstudiantsGrup, maxEstudiantsLaboratori), this);
			llistaSubGrups.add(sgl);
			numEstudiantsGrup -= maxEstudiantsLaboratori;
			codi++;
		}
		
		return numEstudiants -= maxEstudiantsTeoria;
	}

	public ArrayList<SubGrup> getOcupats() {
		ArrayList<SubGrup> sg = new ArrayList<SubGrup>();
		Iterator<SubGrup> it = llistaSubGrups.iterator();
		SubGrup subgrup;
		Classe c;
		while(it.hasNext()){
			subgrup = it.next();
			c = subgrup.getClasse();
			if (!(c == null)) {
				sg.add(subgrup);
			}
		}
		return sg;	
	}
	
	public boolean getFranja() {
		return mati;
	}
	
	@XmlElement
	public int getContadorSolapades() {
		return contadorSolapades;
	}
	
	public void setContadorSolapades(int contadorSolapades) {
		this.contadorSolapades = contadorSolapades;
	}

	@XmlElement
	public int getMinSolapades() {
		return minSolapades;
	}


	public void setMinSolapades(int minSolapades) {
		this.minSolapades = minSolapades;
	}
}
