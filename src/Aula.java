import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Aula implements ItemLlista{
	@XmlElement
	private String idAula;
	@XmlElementWrapper(name = "llista")
	@XmlElement(name = "Classe")
	private ArrayList<Classe> llistaClasses;

	public Aula() {
	}
	
	public Aula(String idAula) {
		this.idAula = idAula;
		llistaClasses = new ArrayList<Classe>();
	}
	
	@Override
	public Object getId() {
		return idAula;
	}
	
	public ArrayList<Classe> getLlistaClasses() {
		return llistaClasses;
	}
	
	public void generarClasse(RangData rangData) {
		Classe classe = new Classe(rangData, this);
		llistaClasses.add(classe);
	}
}
