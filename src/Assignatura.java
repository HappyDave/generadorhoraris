import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Assignatura implements ItemLlista{
	@XmlElement
	private int codi;
	@XmlElement
	private String sigles;
	@XmlElement
	private String nom;
	@XmlElement
	private String practiques;
	@XmlElement
	private int numPlaces;
	
	public Assignatura() {
		
	}
	
	public Assignatura(int id, String nom, String sigles, int numPlaces, String practiques) {
		codi = id;
		this.nom = nom;
		this.sigles = sigles;
		this.numPlaces = numPlaces;
		this.practiques = practiques;
	}

	public String getPractiques() {
		return practiques;
	}

	@Override
	public Object getId() {
		return codi;
	}

	public int getNumPlaces() {
		return numPlaces;
	}

	public String getSigles() {
		return sigles;
	}

	
}
